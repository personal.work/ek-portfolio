import { BrowserRouter, Routes, Route } from 'react-router-dom';




import MainPage from './MainPage';
import Nav from './Nav';



import VisitorList from './visitors/VisitorList';
import VisitorForm from './visitors/VisitorForm'
import VisitorDetail from './visitors/VisitorDetail';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="visits">
            <Route index element={<VisitorList />} />
            <Route path="new" element={<VisitorForm />} />
            <Route path=":id" element={< VisitorDetail />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
