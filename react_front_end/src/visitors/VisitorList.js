import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import './visits.css'


function VisitorList(props) {

    const [visitors, setVisitors] = useState([]);

    async function fetchVisitors() {
        const response = await fetch('http://localhost:8000/visits/')

        if (response.ok){
            const parsedJson = await response.json();
            setVisitors(parsedJson.visitors);
        }
    }


    useEffect(() => {
        fetchVisitors();
    }, []);


    return (
        visitors.map(visitor => {
        return (
            <div class="container">
                <img src="https://cdn.discordapp.com/attachments/465559014395084801/1138885546177998928/note.png" alt="pink sticky note" width="300px" />
                <div className="top-left"><h2>{visitor.last_name}, {visitor.state}</h2></div>
                <div className="centered">{visitor.message}</div>
                <div className="bottom-centered">
                    <Link to={`/visits/${visitor.id}`}>
                        <button className="button-small" type="button">
                            Full Visit...
                        </button>
                    </Link>
                </div>
            </div>
        );
        })
    );
};

    export default VisitorList;
