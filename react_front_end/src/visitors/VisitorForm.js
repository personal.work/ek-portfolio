import React, {useState} from 'react';

function VisitorForm () {

  const [firstName, setFirstName] = useState("")
  const [lastName, setLastName] = useState("")
  const [email, setEmail] = useState("")
  const [message, setMessage] = useState("")
  const [state, setState] = useState("")



  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.email = email;
    data.message = message;
    data.state = state;


    const url = `http://localhost:8000/visits/`;

    const fetchConfig = {
      credentials: 'include',
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFirstName("");
      setLastName("");
      setEmail("");
      setMessage("");
      setState("");
    }
  }

  const handleFirstNameChange = (e) => {
    setFirstName(e.target.value);
  }

  const handleLastNameChange = (e) => {
    setLastName(e.target.value);
  }

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  }

  const handleMessageChange = (e) => {
    setMessage(e.target.value);
  }

  const handleStateChange = (e) => {
    setState(e.target.value)
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Submit an Anonymous Visitor Message!</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleFirstNameChange} value={firstName} placeholder="firstName" required name="firstName" type="text" id="firstName" className="form-control" />
              <label htmlFor="firstName">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleLastNameChange} value={lastName} placeholder="lastName" required name="lastName" type="text" id="lastName" className="form-control" />
              <label htmlFor="lastName">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEmailChange} value={email} placeholder="email" type="email" name="email" id="email" className="form-control" />
              <label htmlFor="email">Email@Company.com</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMessageChange} value={message} placeholder="message" required name="message" type="text" id="message" className="form-control" />
              <label htmlFor="message">Type your anonymous message for Emily...</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStateChange} value={state} placeholder="state" required name="state" type="text" id="state" className="form-control" />
              <label htmlFor="state">State Abbreviation</label>
            </div>
            <button className="btn btn-primary">Send Visit!</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default VisitorForm;
