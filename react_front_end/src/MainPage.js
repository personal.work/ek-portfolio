

function MainPage() {
    return (
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            Need to keep track of your shoes and hats? We have
            the solution for you!
          </p>
          <div class="row">
            <div class="col-sm-6">
              <div class="card">
              <img src="https://hips.hearstapps.com/hmg-prod/images/cushioned-shoes-15408-1632754154.jpg" width="auto" alt="a shoe"/>
                <div class="card-body">
                  <h5 class="card-title">Your List of Shoes</h5>
                  <p class="card-text">Click below to view your list of registered shoes...</p>
                  <a href="/shoes" class="btn btn-primary">Let's Go!</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="card">
              <img src="https://images.pexels.com/photos/35185/hats-fedora-hat-manufacture-stack.jpg?cs=srgb&dl=pexels-pixabay-35185.jpg&fm=jpg" width="auto" alt="a set of hats" />
                <div class="card-body">
                  <h5 class="card-title">Your List of Hats</h5>
                  <p class="card-text">Click below to view your list of registered hats...</p>
                  <a href="/hats" class="btn btn-primary">Let's Go!</a>
                </div>
              </div>
            </div>
          </div>
          <div class="my-sm-3">
              <div class="row">
                <a href="/shoes/new" class="btn btn-primary">Add a New Shoe</a>
                <p>   </p>
                <a href="/hats/new" class="btn btn-primary">Add a New Hat</a>
              </div>
          </div>
        </div>
      </div>
    );
  }

  export default MainPage;
