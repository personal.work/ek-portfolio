from common.json import ModelEncoder
from portfolio_app.models import Visitor

class VisitorListEncoder(ModelEncoder):
    model = Visitor
    properties = [
        "id",
        "last_name",
        "message",
        "state",
    ]

class VisitorDetailEncoder(ModelEncoder):
    model = Visitor
    properties = [
        "id",
        "first_name",
        "last_name",
        "message",
        "state",
        "datetime",
    ]
