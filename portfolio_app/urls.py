from django.urls import path
from .views import list_visitors, detail_visitor

urlpatterns = [
    path("", list_visitors, name="list_visitors"),
    path("<int:id>/", detail_visitor, name="detail_visitor"),
]
