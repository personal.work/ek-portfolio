from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Visitor
from django.http import JsonResponse
import json
from .encoders import VisitorListEncoder, VisitorDetailEncoder


@require_http_methods(["GET", "POST"])
def list_visitors(request):

    ### SHOWS A LIST OF ALL VISITOR MESSAGES ###

    if request.method == "GET":
        visitors = Visitor.objects.all()
        return JsonResponse(
            {"visitors": visitors},
            encoder=VisitorListEncoder
        )

    ### POSTS/CREATES A NEW VISITOR MESSAGE ###

    else:
        content = json.loads(request.body)
        visitor = Visitor.objects.create(**content)
        return JsonResponse(
            visitor,
            encoder=VisitorDetailEncoder,
            safe=False
        )


@require_http_methods(["GET"])
def detail_visitor(request, id):
    if request.method == "GET":

        ### ATTEMPTS TO VIEW A SINGULAR VISITOR'S MESSAGE WITH MORE DETAIL ###

        try:
            visitor = Visitor.objects.get(id=id)
            return JsonResponse(
                {"visitor": visitor},
                encoder = VisitorDetailEncoder,
                safe = False
            )

        ### ALERTS USER TO A 404 ERROR IF MESSAGE DOES NOT EXIST ###

        except Visitor.DoesNotExist:
            response = JsonResponse({"message": "This Visitor's message has been deleted, or never existed!"})
            response.status_code = 404
            return response
