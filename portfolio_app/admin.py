from django.contrib import admin
from .models import Visitor

# Register your models here.
@admin.register(Visitor)
class VisitorAdmin(admin.ModelAdmin):
    list_display = [
        "first_name",
        "last_name",
        "email",
        "message",
        "state",
        "datetime",
    ]
